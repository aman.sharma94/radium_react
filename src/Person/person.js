import React from 'react';
import './person.css';
import Radium from 'radium';

const Person = (props)=> {
    const style = {
        "@media (min-width:300px)":{
            backgroundColor:"skyblue"
        }
    }
    return (
        <div className="Person" style={style} >
            <input id={props.id} onChange={props.changed} name={"name"+props.id} defaultValue={props.name} />
            <p>{props.age}</p>
            <p onClick={props.deleted} >{props.name}</p>
        </div>
    );
}

export default Radium(Person);