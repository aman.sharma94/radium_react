
import './App.css';
import Person from './Person/person';
import React,{ Component } from 'react';
import Radium,{StyleRoot} from 'radium';


class App extends Component{

  state = {
    person:[{
      id:1,
      name:"Aman Sharma",
      age:"26"
    },
    {
      id:2,
      name:"Pardeep Dangi",
      age:"23"
    },
    {
      id:3,
      name:"Himanshu tuteja",
      age:"24"
    }],
    isshow:false
  }
   showHandler = ()=>{
     let ishows = this.state.isshow
    this.setState({
      isshow:!ishows
    })
  }
  nameDeleteHandler = (key)=>{
       let persons = this.state.person;
       delete persons[key];
      this.setState({
        person:persons
      })

  }

  nameChangeHandler = (event)=>{
    let persons = this.state.person;
    let index = event.target.id;
    persons[index].name  = event.target.value;
    this.setState({
      person:persons})
   
  }


render (){
      const style = {
        backgroundColor:"red",
        border: "1px solid skyblue",
        padding: "20px",
        borderRadius: "5%",
        marginTop: "10%",
        fontWeight: "700",
        fontStyle: "italic",
        fontSize: "large",
        ":hover":{
          backgroundColor:"royalblue",
          fontStyle:"normal",
          color:"white"
        }
      }
      let classes = ['Lists'].join(" ");

      let personList = null;
        if(this.state.isshow){
          personList = (
            this.state.person.map((p,index)=>{
             return <Person
              name={p.name}
              age={p.age}
              key={p.id}
              id={index}
              deleted= {()=>this.nameDeleteHandler(index)}
              changed={this.nameChangeHandler}/>
            })
          )
          style.backgroundColor="skyblue";
          classes = ["NewList"].join(" ");
        }
       
  return(
    <StyleRoot>
    <div className="App">
      <p className={classes}>This is list of users</p>
      <button style={style}  onClick={this.showHandler} >Show List</button>
      {personList}
   </div>
   </StyleRoot>
  )
}
}

export default Radium(App);
